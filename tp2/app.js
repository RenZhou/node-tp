const express = require('express')
const app = express()
const fs = require('fs')

let calendar

const users = [
  {
    username: 'user1',
    password: 'password'
  }
]

fs.readFile('./event.json', (err, data) => {
  if (err) throw err
  calendar = JSON.parse(data.toString('UTF8'))
  calendar.map((event) => {
    event.start = new Date(event.start)
    event.end = new Date(event.end)
  })
})

app.set('view engine', 'ejs')

app.get('/', (req, res) => {
  console.log('hello')

  res.render('index', {calendar: calendar})
})

/**
 * Renvoie la liste des ids d'événements
 * @Send : [ id: Number ]
 **/
app.get('/event', (req, res) => {
  res.send(calendar.map((event) => {
    return event.id
  }))
})

/**
 * Renvoie un événement dont l'id a été spécifié en paramètre
 * @Param : id, un nombre
 * @Send : un objet event sérialisé
 **/
app.get('/event/:id', (req, res) => {
  let id = req.params.id

  if (!calendar) {
    res.status(500).send('Internal Server Error')
  }

  try {
    result = calendar.find((event) => {
      return event.id === Number(id)
    })
  } catch (e) {
    res.status(403).send('Forbidden')
  }

  if (result) {
    res.status(200).send(result)
  } else {
    res.status(404).send('Not Found')
  }
})

app.post('/user/login', (req, res, next) => {
  let username = req.query.get('username')
  let password = req.query.get('password')
  console.log(username, password)

  res.status(200).send('Connected')
})

app.listen(3000, () => {
  console.log('Application démarrée sur le port 3000!')
})