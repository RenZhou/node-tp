'use strict'

class Complex {
  constructor (re, im) {
    this.re = re
    this.im = im
  }

  /*
  * @return Complex le carré du complexe this
  */
  square () {
    return new Complex(this.re ** 2 + this.im ** 2 * (-1), 2 * this.re * this.im)
  }

  /*
  * @param {Complex} second à sommer à this
  * @return Complex la somme this + second
  */
  sum (second) {
    return new Complex(this.re + second.re, this.im + second.im)
  }


  /*
  * @return Number le module du complexe this
  */
  module () {
    return Math.sqrt(this.re ** 2 + this.im ** 2)
  }
}

module.exports = Complex