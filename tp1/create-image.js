const readline = require('tp1/readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question('Taille de l\'image à générer ? ', (answer) => {
  let dimension = parseInt(answer)
  console.log('Choix des dimensions : ', `${dimension}x${dimension}px`)
  rl.close()
})
