'use strict'

const Complex = require('./lib/complex')

let c1 = new Complex(0, 1)
console.log("|c1| = " + c1.module())
console.log("c1 * c1 = " + JSON.stringify(c1.square()))
let c2 = new Complex(1, 0)
console.log("c1 + c2 = " + JSON.stringify(c1.sum(c2)))
